var list = [
  { "name" : "Fields",
    "list" : [
      { "name" : "Checkbox" },
      { "name" : "Combobox" },
      { "name" : "File Upload" },
      { "name" : "HTML Area" },
      { "name"  : "Icon"} ,
      { "name" : "Push Button" },
      { "name"  : "Rich Text" },
      { "name" : "Textfield" },
      { "name" : "Textfield_Number" },
      { "name" : "Toggle Button" }
    ]
  },
  { "name"  : "Layouts",
    "list" : [
      { "name" : "Horizontal Layout" },
      { "name" : "Vertical Layout" }
    ]
  },
  { "name"  : "Panels",
    "list" : [
      { "name"  : "Load" },
      { "name"  : "Message" },
      { "name"  : "Modal Window" },
      { "name"  : "Navigation" },
      { "name" : "Panel" },
      { "name"  : "Path" },
      { "name"  : "Table" },
      { "name"  : "Tree" }
    ]
  },
  { "name" : "Examples",
    "list" : [
      { "name"  : "Bikini Project Details" },
      { "name"  : "Fixpayment Bills" },
      { "name"  : "Vsezagorod Back-end" }
    ]
  }
];

angular.module("App", ["ui.router"])

.config(["$logProvider","$stateProvider", "$urlRouterProvider", "$locationProvider",
function config($logProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

  $logProvider.debugEnabled(true);
  $locationProvider.html5Mode({ enabled: true, requireBase: false });
  $urlRouterProvider.otherwise( "home" );
  
  $stateProvider.state("home", {
    url: '/'
  });
  
}])

.controller("SkeletonCtrl", function SkeletonCtrl($log, $scope) {
  
  $scope.list = list;
  //$scope.del = delayedData[0];
  //$log.debug("Del:", $scope.del);
  
  for (var i = 0; i < $scope.list.length; i++) {
    $log.debug("views/"+$scope.list[i].name.replace(/ /g,'_').toLowerCase());
    for (var j = 0; j < $scope.list[i].list.length; j++) {
      $scope.list[i].list[j].view = "views/" + ($scope.list[i].name.replace(/ /g,'_') + "/" + $scope.list[i].list[j].name.replace(/ /g,'_')).toLowerCase() + ".view.html";
      console.log($scope.list[i].list[j].view);
    }
  }
  $scope.getSrc = function(itemView) {
    return itemView;
  };
  $scope.clickNav = function(itemName) {
    $scope.activeItem=itemName;
    $log.debug($scope.activeItem);
  };
  
  $log.debug();
  
})
.controller("ItemCtrl", function ItemCtrl($log, $scope) {
  
  
})
;

