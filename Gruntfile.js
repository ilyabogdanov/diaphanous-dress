module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    clean: {
        dist_views_tt: [ "dist/triangular-tanga-0.1.0" ],
      dist_views: [ "dist/th-0.9.0" ],
      test_pkg: [ ".test/css" ],
      test_views: [ ".test/views" ]
    },
    connect: {
      server: {
        options: {
          port: 8000,
          base: ".test",
          protocol: "http",
          hostname: "127.0.0.1",
          options: { livereload: true }
        }
      }
    },
    copy: {
      bower_components: {
        files: [{
          expand: true,
          cwd: "bower_components",
          dest: ".test/bower_components",
          src: [
            "angular/angular.min.js",
            "angular-ui-router/release/angular-ui-router.min.js"
          ]}
        ]
      },
      test_pkg: {
        files: [{
          expand: true,
          cwd: "dist/css",
          dest: ".test/css",
          src: [
            "**/*"
          ]}
        ]
      },
      test_html: {
        files: [{
          expand: true,
          cwd: "test",
          dest: ".test",
          src: [
            "index.html"
          ]}
        ]
      },
      test_js: {
        files: [{
          expand: true,
          cwd: "test",
          dest: ".test",
          src: [
            "test.js"
          ]}
        ]
      }
    },
    haml: {
        dist_views_tt: {
            files: [{
                cwd: "src/triangular-tanga-0.1.0",
                dest: "dist/triangular-tanga-0.1.0",
                src: "**/*.haml",
                ext: ".view.html",
                expand: true
            }]
        },
      dist_views_lang: {
        files: [{
          cwd: "src/th-0.9.0/lang",
          dest: "dist/th-0.9.0/th/lang",
          src: "**/*.haml",
          ext: ".view.html",
          expand: true
        }]
      },
      dist_views_ui: {
        files: [{
          cwd: "src/th-0.9.0/ui",
          dest: "dist/th-0.9.0/th/ui",
          src: "**/*.haml",
          ext: ".view.html",
          expand: true
        }]
      },
      test_views: {
        files: [{
          cwd: "test/views",
          dest: ".test/views",
          src: "**/*.haml",
          ext: ".view.html",
          expand: true
        }]
      }
    },
    sass: {
      dist: {
        options: { sourcemap: "none" },
        files: [
          { "dist/css/stylesheet.css": "src/sass/config.scss" }
        ]
      },
      test: {
        options: { sourcemap: "none" },
        files: [
          { ".test/test.css": "test/test.scss" }
        ]
      }
    },
    watch: {
      dist_sass: {
        files: [ "src/sass/**/*.scss", "src/sass/**/*.sass" ], tasks: [ "sass:dist", "clean:test_pkg", "copy:test_pkg" ]
      },
        dist_views_tt: {
            files: [ "src/triangular-tanga-0.1.0/**/*.haml" ], tasks: [ "clean:dist_views_tt", "haml:dist_views_tt" ]
        },
      dist_views: {
        files: [ "src/th-0.9.0/**/*.haml" ], tasks: [ "clean:dist_views", "haml:dist_views_lang", "haml:dist_views_ui" ]
      },
      test_sass: {
        files: [ "test/test.scss"  ], tasks: [ "sass:test" ]
      },
      test_haml: {
        files: [ "test/views/**/*.haml" ], tasks: [ "clean:test_views", "haml:test_views" ]
      },
      test_html: {
        files: [ "test/index.html" ], tasks: [ "copy:test_html" ]
      },
      test_js: {
        files: [ "test/test.js" ], tasks: [ "copy:test_js" ]
      }
    },
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks("grunt-contrib-connect");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-haml2html");
  grunt.registerTask("default", ['connect',"watch"]);
}